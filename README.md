# Medium Frequency Trading

A project in which I try to get some time-series forecasting ideas out of my head and into my real world.

## The General Idea


| Time series | -> | Black Box Forecasting Model | -> | Prediction |

Options:
- Lengths of time series
- Interval of time series
- Black Box models
- Model parameters
        
Statistics:
- 2D representation of [Lengths, Intervals] with 3rd dimension the performance

Performance:
- Combination of time series
- Deviation of prediction from actual price

Properties:
- Proportionally divide assets w.r.t. prediction
- Choose time series with best predictability
- Compute the differences for buy/sell
- OPTIONAL: factor in minimal amounts
- Keep record of all trades and visually represent profits/loss for every time series

Random Thoughts:
- Instead of absolute value, try to predict differences
- Treat all deviations as bad, as to try to get the best predicting models, not the 'I got lucky and chose a coin that jumped much higher than predicted' models
- Make a second repository which just gets updated after every trade with statistics
- Make a class Model that gets implemented by the different models. Add hyperparameters as arguments of the constructor
